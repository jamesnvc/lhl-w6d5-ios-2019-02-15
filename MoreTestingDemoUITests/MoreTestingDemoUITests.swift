//
//  MoreTestingDemoUITests.swift
//  MoreTestingDemoUITests
//
//  Created by James Cash on 15-02-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import XCTest

class MoreTestingDemoUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.

        let app = XCUIApplication()
        XCTAssert(app.staticTexts["First Page"].exists)
        app/*@START_MENU_TOKEN@*/.buttons["NextButton"]/*[[".buttons[\"Next Button\"]",".buttons[\"NextButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssert(app.staticTexts["Second Page"].exists)
        app.buttons["NextButton"].tap()
        XCTAssert(app.staticTexts["Third Page"].exists)
    }

    func testEXampleTwo() {
        let app = XCUIApplication()
        app/*@START_MENU_TOKEN@*/.buttons["NextButton"]/*[[".buttons[\"Next Button\"]",".buttons[\"NextButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        XCTAssert(app.staticTexts["Second Page"].exists)
        app/*@START_MENU_TOKEN@*/.staticTexts["PageLabel"].press(forDuration: 0.8);/*[[".staticTexts[\"Second Page\"]",".tap()",".press(forDuration: 0.8);",".staticTexts[\"PageLabel\"]"],[[[-1,3,1],[-1,0,1]],[[-1,2],[-1,1]]],[0,0]]@END_MENU_TOKEN@*/
    }
}
