//
//  Math.swift
//  MoreTestingDemo
//
//  Created by James Cash on 15-02-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

import Foundation

struct Math {
    // 1, 1, 2, 3, 5, 8, 13, 21
    static func naive_fib(_ n: Int) -> Int {
        if n == 1 { return 1 }
        if n == 2 { return 1 }
        return naive_fib(n - 1) + naive_fib(n - 2)
    }

    static var cache = [1: 1, 2: 1]
    static func memo_fib(_ n: Int) -> Int {
        if let f = cache[n] {
            return f
        }
        let f = memo_fib(n - 1) + memo_fib(n - 2)
        cache[n] = f
        return f
    }

    static func closed_fib(_ n: Int) -> Int {
        let phi = (1 + sqrt(5))/2
        let psi = 1 - phi
        return Int((pow(phi, Double(n)) - pow(psi, Double(n)))/(phi - psi))
    }

    static let fib = closed_fib
}
